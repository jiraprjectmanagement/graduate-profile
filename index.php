<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->
<section class="profile-area">
   <div class="contain">
      <div class="profile-inner">
         <div class="profile-details">
            <img src="img/profile-img.jpg" alt="">
            <div class="profile-bottom">
               <a href="" class="video-icon gardi-dflt">
                  <img src="img/play-icon.svg" alt="">
                  Watch Video
               </a>
               <div class="prof-content">
                  <h5>Abdul Wasay Usmani</h5>
                  <p>Computer Science</p>
                  <p>Class of 2021</p>
               </div>
               <ul class="prof-social">
                  <li>
                     <a href="mailto:studentname@gmail.com">
                     <img src="img/email.svg" alt="">
                     studentname@gmail.com
                     </a>
                  </li>
                  <li>
                     <a href="javascript:;">
                     <img src="img/link.svg" alt="">
                     linkdin.com/studentname
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="aspriration-statement">
            <div class="aspiration-statment">
               <h3>Aspiration Statement</h3>
               <p>I want to persue my  masters in the field of public health.Before i go for my masters, I wants to work  in the field  
                  to get more exposure and understand it better.  </p>
            </div>
            <div class="statement-box">
               <h5>Core Skills</h5>
               <ul class="skills-items">
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> C, C++ , C# </li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Python</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> .Net</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Java</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Sql</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> .Net</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Web development</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> C, C++ , C# </li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Python</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> .Net</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Java</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Sql</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> .Net</li>
                  <li class="gardi-dflt"> <img src="img/tick-icon.svg" alt=""> Web development</li>
               </ul>
            </div>
            <div class="statement-box">
               <h5>Academic Awards/Achievements</h5>
               <ul class="skills-items dir-cloumn">
                  <li> <img src="img/tick-icon.svg" alt=""> HU TOPS 100% Scholarship </li>
               </ul>
            </div>
            <div class="statement-box">
               <h4>Experience</h4>
               <h5>Leadership Experience/Meta-curricular</h5>
               <ul class="skills-items dir-cloumn">
                  <li> <img src="img/tick-icon.svg" alt=""> Orientation Leader For the Batch of 2024 (2020) </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->