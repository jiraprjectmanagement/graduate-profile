
<!DOCTYPE html>
<html lang="en">
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <title>Graduate Profile</title>
</head>
<body>

<header>
    <div class="contain">
        <div class="header-inner">
            <div class="logo">
                <img src="img/logo.svg" alt="">
            </div>
            <div class="header-right">
                <ul class="menu-list">
                    <li>
                        <a href="javascript:;">Home</a>
                    </li>   
                    <li>
                        <a href="javascript:;">Contact us</a>
                    </li>   
                </ul>
                <a href="javascript:;" class="gardi-dflt">
                        Visit HU Website
                </a>
            </div>
        </div>
    </div>
</header>


